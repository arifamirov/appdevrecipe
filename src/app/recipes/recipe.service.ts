import { Recipe } from './recipe.model';
import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    // private recipes: Recipe[] = [
    //     new Recipe('A test recipe', 'This is simply a test',
    //     'https://img-s-msn-com.akamaized.net/tenant/amp/entityid/AAFA7tS.img?h=350&w=624&m=6&q=60&u=t&o=t&l=f&f=jpg&x=592&y=547', [
    //         new Ingredient('Meat', 1),
    //         new Ingredient ('French Fries', 20)
    //     ]),
    //     new Recipe('Another test recipe', 'This is simply a test',
    //     'https://img-s-msn-com.akamaized.net/tenant/amp/entityid/AAFA7tS.img?h=350&w=624&m=6&q=60&u=t&o=t&l=f&f=jpg&x=592&y=547', [
    //         new Ingredient('Buns', 2),
    //         new Ingredient('Meat', 1)
    //     ])
    // ];

    private recipes: Recipe[] = [];

    constructor(private slService: ShoppingListService) {}

    setRecipes(recipes: Recipe[]) {
      this.recipes = recipes;
      this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }
}

